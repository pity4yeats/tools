This project is licensed under GPL-2.0.  For a copy of a the license, please
see the LICENSE file.

Users making changes must include a "Signed-off-by:" tag on all commits that
acknowledges the DCO, https://developercertificate.org.

There are additional tools maintained by various developers within Red
Hat that may also be of interest.

Additional tools:
=================
- **rhpatchreview** is a python and dialog based cli patch review tool that accepts
  merge request URLs or Red Hat project names as it's primary input, and walks
  the user through review of the given merge request, with assorted automated
  checks for bug data, upstream commit references, per-patch vimdiff viewing
  options, and an interface for leaving feedback on the merge request, among
  other features.

  https://gitlab.com/jwilsonrh/rhpatchreview
